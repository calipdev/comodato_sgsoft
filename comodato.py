# -*- coding: utf-8 -*-
##############################################################################
#
#    OpenERP, Open Source Management Solution
#    Copyright (C) 2004-2013 Tiny SPRL (<http://tiny.be>).
#
#    This program is free software: you can redistribute it and/or modify
#    it under the terms of the GNU Affero General Public License as
#    published by the Free Software Foundation, either version 3 of the
#    License, or (at your option) any later version.
#
#    This program is distributed in the hope that it will be useful,
#    but WITHOUT ANY WARRANTY; without even the implied warranty of
#    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#    GNU Affero General Public License for more details.
#
#    You should have received a copy of the GNU Affero General Public License
#    along with this program.  If not, see <http://www.gnu.org/licenses/>.
#
##############################################################################
import time
import datetime

from openerp.osv import fields, osv
from openerp.tools.translate import _

class product_template(osv.osv):
    _inherit = 'product.template'

    _columns = {
        'comodato': fields.boolean('Puede ser Prestado', help="Tick if you want to create contract to manage sales after service."),
    }

product_template()

class product_product(osv.osv):
    _name = 'product.product'
    _inherit = 'product.product'

    def get_stock_locations(self, cr, uid, ids, field_names=None, arg=None, context=None):
        result = {}
        if not ids: return result
        context['only_with_stock'] = True
        for id in ids:
            context['product_id'] = id
            location_obj = self.pool.get('stock.location')
            result[id] = location_obj.search(cr, uid, [('usage', '=', 'internal')], context=context)
        return result

    _columns = {
        'stock_locations': fields.function(get_stock_locations, type='one2many', relation='stock.location', string='Stock by Location'),
    }

product_product()

class comodato_contract(osv.Model):
    _name = "comodato.contract"    
    _columns = {
        'nombre': fields.char('Nombre',size=30,readonly=True),
        'product_id': fields.many2one('product.product','Producto', readonly=True),
        'partner_id': fields.many2one( 'res.partner', 'Cliente',readonly=True), 
        'observacion': fields.text('Observaciones'),
        'fecha_inicio': fields.date('Fecha Inicio'),
        'fecha_fin': fields.date('Fecha Fin'),
        'prestado': fields.boolean('Prestado/Devuelto'),
        'sale_id': fields.many2one('sale.order', string="Orden de Venta", readonly=True),
        'delivery_id': fields.many2one('stock.picking.out', string="Orden de Entrega", readonly=True),
        'prodlot_id': fields.many2one('stock.production.lot', "Numero de Serie",readonly=True),     
    }
    _defaults = {'prestado':True,}

    def copy(self, cr, uid, id, default=None, context=None):
        default = default or {}
        default.update({'sale_id' : False})
        default.update({'delivery_id' : False})
        return super(comodato_contract, self).copy(cr, uid, id, default, context) 
        
    def devolver_comodato(self, cr, uid, vals, context={}):
        sale_id = context.get('sale_id')
        id = context.get('id')        
        product_id = context.get('product_id')
        prodlot_id = context.get('prodlot_id')
        delivery_id = context.get('delivery_id')         
        print "llamar ventana o flujo de pedidos de ventas
        
    def checar_devolucion(self, cr, uid, ids, context):
        origin= None
        sale_id = context.get('sale_id')
        id = context.get('id')        
        product_id = context.get('product_id')
        prodlot_id = context.get('prodlot_id')
        delivery_id = context.get('delivery_id')            
        query_sql = "select count(id) from stock_move where (origin = %s and product_id = %d and prodlot_id = %d and state = 'done')"%(origin,product_id,prodlot_id) 
        cr.execute(query_sql)  
        
comodato_contract()

class comodato_solicitud(osv.Model):
    _name = "comodato.solicitud"    
    _columns = {
        'create_date': fields.datetime('Fecha Elaboracion', readonly=True),
        'product_id': fields.many2one('product.product','Producto'),
        'comd_sol_prod_ids': fields.one2many('comodato.solicitud.productos','comodato_solicitud_id','Productos'),      
        'partner_id': fields.many2one( 'res.partner', 'Cliente'), 
        'observacion_venta': fields.text('Observaciones Ventas'),
        'observacion_control': fields.text('Observaciones Control Comodatos'),
        'razon': fields.text('Razon'),
        'prodlot_id': fields.many2one('stock.production.lot', "Numero de Serie"), 
        'create_uid': fields.many2one('res.users', "Elaborador", readonly=True), 
        'state': fields.selection([
            ('solicitado','Solicitado'),
            ('autorizado','Autorizado'),
            ('prestado','Prestado'),
            ('denegado','Denagado'),
            ('terminado','Terminado'),
            ],'Status', select=True, readonly=True, track_visibility='onchange',
            help=' * The \'Draft\' status is used when a user is encoding a new and unconfirmed Invoice. \
            \n* The \'Pro-forma\' when invoice is in Pro-forma status,invoice does not have an invoice number. \
            \n* The \'Open\' status is used when user create invoice,a invoice number is generated.Its in open status till user does not pay invoice. \
            \n* The \'Paid\' status is set automatically when the invoice is paid. Its related journal entries may or may not be reconciled. \
            \n* The \'Cancelled\' status is used when user cancel invoice.'),
    }
    def prestar_comodato(self, cr, uid, vals, context={}):
        print "llamar ventana o flujo de pedidos de ventas"
        
    def action_notificar_responsable(self, cr, uid, context=None):
        print "notificado responsable"
        
    def action_notificar_encargado(self, cr, uid, context=None):
        print "notificado encargado"

    def action_production_lots_form(self, cr, uid, ids, context=None):
        if context is None: context = {}
        if context.get('active_model') != self._name:
            context.update(active_ids=ids, active_model=self._name)
        partial_id = self.pool.get("stock.partial.move").create(
            cr, uid, {}, context=context)
        return {
            'name':_("Products to Process"),
            'view_mode': 'form',
            'view_id': False,
            'view_type': 'form',
            'res_model': 'stock.partial.move',
            'res_id': partial_id,
            'type': 'ir.actions.act_window',
            'nodestroy': True,
            'target': 'new',
            'domain': '[]',
            'context': context
        }
        
comodato_solicitud()

class comodato_solicitud_productos(osv.Model):
    _name = "comodato.solicitud.productos"    
    _columns = {        
        'product_id': fields.many2one('product.product','Producto'),  
        'prodlot_id': fields.many2one('stock.production.lot', "Numero de Serie"),
        'comodato_solicitud_id': fields.many2one('comodato.solicitud','Comodato Solicitud'),        
    }    
comodato_solicitud_productos()

class stock_partial_picking(osv.osv):
    _inherit = 'stock.partial.picking'

    def do_partial(self, cr, uid, ids, context=None):
        if context is None:
            context = {}
        prod_pool = self.pool.get('product.product')
        cont_pool = self.pool.get('account.analytic.account')
        #agregado
        comodato_pool = self.pool.get('comodato.contract')
        stock_pool = self.pool.get('stock.picking.out')
        prodlot_obj = self.pool.get('stock.production.lot')
        picking_obj = self.browse(cr, uid, ids[0], context=context)
        delivery_id = context.get('active_id', False)
        
        if not delivery_id:
            return super(stock_partial_picking, self).do_partial(cr, uid, ids, context=context)
        delivery_order = stock_pool.browse(cr, uid, [delivery_id], context=context)[0]
        picking_type = picking_obj.picking_id.type
        manager_id = False

        if not (picking_type == 'out'):
            return super(stock_partial_picking, self).do_partial(cr, uid, ids, context=context)
            
        if delivery_order.sale_id:
            if delivery_order.sale_id.section_id and delivery_order.sale_id.section_id.user_id:
                manager_id =  delivery_order.sale_id.section_id.user_id.id
            else:
                manager_id = delivery_order.sale_id.user_id.id
                
        saldo_comodato = delivery_order.sale_id.amount_total        
        if saldo_comodato == 0.0:
            for wizard_line in picking_obj.move_ids:
                prod_obj = prod_pool.browse(cr, uid, [wizard_line.product_id.id], context=context)[0]
                if prod_obj.comodato and wizard_line.prodlot_id:
                    vals2 = {
                        'name' : wizard_line.product_id.name + ' (%s)' % wizard_line.prodlot_id.name ,
                        'code': wizard_line.prodlot_id.name,
                        'type' : 'contract',
                        'partner_id': delivery_order.partner_id and delivery_order.partner_id.id or False,
                        'date_start' : time.strftime('%Y-%m-%d'),
                        'company_id' : delivery_order.company_id.id,
                        'manager_id' : manager_id,
                        'sale_id' : delivery_order.sale_id.id,
                        'delivery_id' : delivery_id or False
                     }
                    vals = {
                        'nombre' : wizard_line.product_id.name + ' (%s)' % wizard_line.prodlot_id.name ,
                        'product_id' : wizard_line.product_id.id,                        
                        'sale_id' : delivery_order.sale_id.id,
                        'delivery_id' : delivery_id or False,
                        'partner_id': delivery_order.partner_id.id or False,                        
                        'fecha_inicio': datetime.datetime.now(),
                        'prodlot_id': wizard_line.prodlot_id.id,
                     }

                    comodato_id = comodato_pool.create(cr, uid, vals, context=context)
                    prodlot_obj.write(cr, uid, [wizard_line.prodlot_id.id], {'comodato_id' : comodato_id}, context=context)
        return super(stock_partial_picking, self).do_partial(cr, uid, ids, context=context)
    
    def get_saldo_comodato(self,cr,uid, ids,context,sale_id):
        sale_obj = self.pool.get('sale.order')
        return sale_obj.browse(cr, uid, sale_id).amount_total

stock_partial_picking()

class stock_production_lot(osv.osv):
    _inherit = 'stock.production.lot'    
    _columns = {
        'comodato_id': fields.many2one('comodato.contract', string="Prestamo", readonly=True, help="Referencia de Comodato ha sido creado con el prestamo."),
    }
    
    def copy(self, cr, uid, id, default=None, context=None):
        default = default or {}
        default.update({'comodato_id' : False})
        return super(stock_production_lot, self).copy(cr, uid, id, default, context)
    
stock_production_lot()

# vim:expandtab:smartindent:tabstop=4:softtabstop=4:shiftwidth=4:
